package org.randombits.confluence.intercom;

public interface StubConnection extends Connection {
    String getValue();
}
