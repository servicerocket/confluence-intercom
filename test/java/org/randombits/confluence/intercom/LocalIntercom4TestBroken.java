package org.randombits.confluence.intercom;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.facade.FacadeAssistant;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;

public class LocalIntercom4TestBroken {
    private Mockery context = new JUnit4Mockery();

    private LocalIntercom4 intercom;

    private ContainerContext containerContext;

    private PluginAccessor pluginAccessor;

    private List<Plugin> plugins;

    private Map<Plugin, ClassLoader> pluginLoaders;

    @Before
    public void setUp() throws Exception {
        containerContext = context.mock( ContainerContext.class );
        ContainerManager.getInstance().setContainerContext( containerContext );

        pluginAccessor = context.mock( PluginAccessor.class );
        expectPluginAccessor();

        plugins = new java.util.ArrayList<Plugin>();
        pluginLoaders = new java.util.HashMap<Plugin, ClassLoader>();

        intercom = new LocalIntercom4();
    }

    /**
     * Creates the plugin and registers it with the plugin list, and creates a
     * test classloader for it.
     * 
     * @param name
     *            The name of the mock object.
     * @return The new plugin.
     */
    private Plugin createPlugin( String name ) {
        final Plugin plugin = context.mock( Plugin.class, name );
        ClassLoader loader = new IsolatedClassLoader().include( getClass().getPackage(), FacadeAssistant.class
                .getPackage() );
        pluginLoaders.put( plugin, loader );
        plugins.add( plugin );

        context.checking( new Expectations() {
            {
                allowing( plugin ).isDynamicallyLoaded();
                will( returnValue( true ) );
            }
        } );

        return plugin;
    }

    private Class<?> loadClass( Plugin plugin, String name, Class<?> clazz ) throws ClassNotFoundException {
        Class<?> result = null;
        ClassLoader classLoader = pluginLoaders.get( plugin );
        try {
            result = classLoader.loadClass( name );
        } catch ( ClassNotFoundException e ) {
        }

        if ( result == null && clazz != null )
            return clazz.getClassLoader().loadClass( name );

        return result;
    }

    private void expectPluginAccessor() {
        context.checking( new Expectations() {
            {
                allowing( containerContext ).getComponent( "pluginAccessor" );
                will( returnValue( pluginAccessor ) );
            }
        } );
    }

    @After
    public void tearDown() throws Exception {
        intercom = null;
        plugins.clear();
        plugins = null;
        pluginLoaders.clear();
        pluginLoaders = null;
    }

    @Test
    public void testGetInstance() {
        assertSame( LocalIntercom4.getInstance(), LocalIntercom4.getInstance() );
        assertNotSame( intercom, LocalIntercom4.getInstance() );
    }

    @Test
    public void testAddLocalBundle() throws ClassNotFoundException {
        Connection c1 = context.mock( StubConnection.class );

        LocalConnectionBundle bundle = new LocalConnectionBundle( c1 );

        final Plugin plugin1 = createPlugin( "plugin1" );

        context.checking( new Expectations() {
            {
                one( pluginAccessor ).getPlugins();
                will( returnValue( plugins ) );

                one( plugin1 ).loadClass( LocalIntercom4.class.getName(), Object.class );
                will( returnValue( loadClass( plugin1, LocalIntercom4.class.getName(), Object.class ) ) );
            }
        } );

        intercom.addLocalBundle( bundle );
        assertEquals( 1, intercom.getLocalBundles().length );
        //assertEquals( true, intercom.getLocalBundles().contains( bundle ) );
        assertEquals( true, intercom.removeLocalBundle( bundle ) );
        assertEquals( 0, intercom.getLocalBundles().length );
        //assertEquals( false, intercom.getLocalBundles().contains( bundle ) );
    }

    /*
    @Test
    public void testAddRemoteBundle() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testGetRemoteBundles() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testRemoveRemoteBundle() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testGetRemoteIntercoms() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testEnable() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testDisable() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testRegisterIntercom() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testDeregisterIntercom() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testGetPlugin() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testAddLocalIntercomListener() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testRemoveLocalIntercomListener() {
        fail( "Not yet implemented" );
    }

    @Test
    public void testFindConnections() {
        fail( "Not yet implemented" );
    }
    */
}
