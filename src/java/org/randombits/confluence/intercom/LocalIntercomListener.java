package org.randombits.confluence.intercom;

/**
 * This class can be used to be notified of when remote Intercoms are added or
 * removed from the local intercom.
 * 
 * @author David Peterson
 * @see LocalIntercom4#addLocalIntercomListener(LocalIntercomListener)
 */
public interface LocalIntercomListener {
    /**
     * This method is called after a connection bundle is added to the intercom
     * being listened to.
     * 
     * @param bundle
     *            the new connection bundle.
     */
    void addedConnectionBundle( ConnectionBundle bundle );

    /**
     * This method is called after a connection bundle is removed from the
     * intercom being listened to.
     * 
     * @param bundle
     *            the removed connection bundle.
     */
    void removedConnectionBundle( ConnectionBundle bundle );
}
