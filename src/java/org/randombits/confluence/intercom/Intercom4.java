/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.intercom;

import org.randombits.facade.Facadable;

/**
 * This interface defines the basic abilities of the Intercom system.
 * 
 * @author David Peterson
 */
@Facadable
public interface Intercom4 {
    /**
     * Registers the specified intercom with this intercom.
     * 
     * @param intercom
     *            The intercom to register.
     * @return <code>false</code> if the intercom was already registered.
     */
    boolean registerIntercom( Intercom4 intercom );

    /**
     * Deregisters the specified intercom from this intercom.
     * 
     * @param intercom
     *            The intercom to deregister.
     * @return <code>false</code> if the intercom was already deregistered.
     */
    boolean deregisterIntercom( Intercom4 intercom );

    /**
     * Registers an additional connection bundle. If the bundle had already been
     * added, the method returns <code>false</code>.
     * 
     * @param bundle
     *            The connection bundle.
     * @param intercom
     *            The source intercom.
     * @return <code>true</code> if the connection bundle was added.
     */
    boolean addRemoteBundle( ConnectionBundle bundle );

    /**
     * Removes the bundle from the intercom. If the bundle had been present and
     * was successfully removed, <code>true</code> is returned.
     * 
     * @param bundle
     *            The bundle to remove.
     * @return <code>true</code> if successful.
     */
    boolean removeRemoteBundle( ConnectionBundle bundle );

    /**
     * Returns in immutable array of the bundles which are registered directly
     * with the intercom.
     * 
     * @return The set of connection bundles.
     */
    ConnectionBundle[] getLocalBundles();
}
