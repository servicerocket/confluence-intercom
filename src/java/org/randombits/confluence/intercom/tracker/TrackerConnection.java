package org.randombits.confluence.intercom.tracker;

import org.randombits.confluence.intercom.Connection;

/**
 * Implementors should provide an interface subclass of this class, as well as a
 * specific type to return.
 * 
 * @author David Peterson
 */
public interface TrackerConnection extends Connection {
    /**
     * Returns the object being provided if it supports the specified type. If
     * not, <code>null</code> is returned.
     * 
     * @param The
     *            type of value required.
     * @return The value being provided.
     */
    <T> T getValue( Class<T> type );
}
