package org.randombits.confluence.intercom.tracker;

public interface TrackerListener<Value> {
    /**
     * Called when a new value is provided, usually via adding a connection.
     * 
     * @param value
     *            The new value.
     */
    public void valueAdded( Value value );

    /**
     * Called when a tracked value is removed, usually via removing a
     * connection.
     * 
     * @param value
     *            The removed value.
     */
    public void valueRemoved( Value value );
}
