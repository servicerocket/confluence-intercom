package org.randombits.confluence.intercom.tracker;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import org.randombits.confluence.intercom.ConnectionBundle;
import org.randombits.confluence.intercom.LocalIntercom4;
import org.randombits.confluence.intercom.LocalIntercomListener;

/**
 * This class provides support for the common case of plugins wishing to publish
 * services to other plugins, or subscribe to specific services provided by
 * other plugins.
 * 
 * @author David Peterson
 * @param <Value>
 *            The value type.
 */
public class Tracker<Value> implements LocalIntercomListener, Iterable<Value> {

    private final Class<Value> valueType;

    private final Comparator<? super Value> comparator;

    private final Filter<? super Value> filter;

    private final Set<Value> mValues;

    private final Set<Value> uValues;

    private final SortedSet<Value> uSortedValues;

    private List<TrackerListener<? super Value>> listeners;

    public Tracker( Class<Value> thingType ) {
        this( thingType, null, null );
    }

    public Tracker( Class<Value> thingType, Filter<? super Value> filter ) {
        this( thingType, null, filter );
    }

    public Tracker( Class<Value> thingType, Comparator<? super Value> comparator ) {
        this( thingType, comparator, null );
    }

    public Tracker( Class<Value> valueType, Comparator<? super Value> comparator, Filter<? super Value> filter ) {

        this.valueType = valueType;
        this.comparator = comparator;
        this.filter = filter;

        if ( comparator == null ) {
            mValues = new java.util.HashSet<Value>();
            uSortedValues = null;
        } else {
            SortedSet<Value> sortedValues = new java.util.TreeSet<Value>( comparator );
            mValues = sortedValues;
            uSortedValues = Collections.unmodifiableSortedSet( sortedValues );
        }
        uValues = Collections.unmodifiableSet( mValues );

        LocalIntercom4 intercom = LocalIntercom4.getInstance();
        synchronized ( intercom ) {
            addValues( intercom.findConnections( TrackerConnection.class ) );
            intercom.addLocalIntercomListener( this );
        }
    }

    /**
     * Returns the comparator being used, if present.
     * 
     * @return The comparator.
     */
    public Comparator<? super Value> getComparator() {
        return comparator;
    }

    /**
     * Adds a listener that will be notified if any things are added or removed.
     * 
     * @param listener
     *            The listener to add.
     */
    public void addListener( TrackerListener<? super Value> listener ) {
        if ( listeners == null )
            listeners = new java.util.ArrayList<TrackerListener<? super Value>>();

        listeners.add( listener );
    }

    /**
     * Removes the specified listener.
     * 
     * @param listener
     *            The listener to remove.
     * @return <code>true</code> if the listener was present and has now been
     *         removed.
     */
    public boolean removeListener( TrackerListener<? super Value> listener ) {
        if ( listeners == null )
            return false;

        return listeners.remove( listener );
    }

    private void addValues( Collection<TrackerConnection> connections ) {
        if ( connections != null ) {
            for ( TrackerConnection p : connections ) {
                addValue( p.getValue( valueType ) );
            }
        }
    }

    private void addValues( TrackerConnection... connections ) {
        if ( connections != null ) {
            for ( TrackerConnection p : connections ) {
                addValue( p.getValue( valueType ) );
            }
        }
    }

    private void addValue( Value value ) {
        if ( value != null && ( filter == null || filter.matches( value ) ) && mValues.add( value ) ) {
            if ( listeners != null ) {
                for ( TrackerListener<? super Value> listener : listeners )
                    listener.valueAdded( value );
            }

        }
    }

    /**
     * Returns an unmodifiable set of the values found.
     * 
     * @return
     */
    public Set<Value> getValues() {
        return uValues;
    }

    /**
     * Returns an unmodifiable sorted set of the values found, if this finder
     * has a comparator. Otherwise, returns null.
     * 
     * @return The SortedSet of values.
     */
    public SortedSet<Value> getSortedValues() {
        return uSortedValues;
    }

    public Class<Value> getValueType() {
        return valueType;
    }

    public synchronized void addedConnectionBundle( ConnectionBundle bundle ) {
        addValues( bundle.getConnections( TrackerConnection.class ) );
    }

    public synchronized void removedConnectionBundle( ConnectionBundle bundle ) {
        removeValues( bundle.getConnections( TrackerConnection.class ) );
    }

    private void removeValues( TrackerConnection... connections ) {
        if ( connections != null ) {
            for ( TrackerConnection p : connections ) {
                removeValue( p.getValue( valueType ) );
            }
        }
    }

    private void removeValue( Value value ) {
        if ( value != null && mValues.remove( value ) ) {
            if ( listeners != null ) {
                for ( TrackerListener<? super Value> listener : listeners )
                    listener.valueRemoved( value );
            }
        }
    }

    public Iterator<Value> iterator() {
        return getValues().iterator();
    }
}
