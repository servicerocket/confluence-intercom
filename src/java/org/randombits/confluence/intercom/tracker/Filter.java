package org.randombits.confluence.intercom.tracker;

/**
 * Filters check if a value matches the specified requirements. Implement the
 * {@link #matches(Object)} to create your own filter.
 * 
 * @author David Peterson
 * 
 * @param <Value>
 *            The type for this filter.
 */
public interface Filter<Value> {

    /**
     * Returns <code>true</code> if this filter matches the specified value.
     * 
     * @param value
     *            The value to test.
     * @return <code>true</code> if the value matches.
     */
    public boolean matches( Value value );

    /**
     * This is a utility class containing static methods which aid with common
     * filter types.
     * 
     * @author David Peterson
     * 
     */
    public static final class With {
        private With() {
        }

        public static <Value> Filter<Value> or( final Filter<? super Value>... filters ) {
            return new Filter<Value>() {
                public boolean matches( Value value ) {
                    for ( Filter<? super Value> f : filters ) {
                        if ( f.matches( value ) )
                            return true;
                    }
                    return false;
                }
            };
        }

        public static <Value> Filter<Value> and( final Filter<? super Value>... filters ) {
            return new Filter<Value>() {
                public boolean matches( Value value ) {
                    for ( Filter<? super Value> f : filters ) {
                        if ( !f.matches( value ) )
                            return false;
                    }
                    return true;
                }
            };
        }

        public static <Value> Filter<Value> not( final Filter<? super Value> filter ) {
            return new Filter<Value>() {
                public boolean matches( Value value ) {
                    return !filter.matches( value );
                }
            };
        }
    }
}
