package org.randombits.confluence.intercom.tracker;

import org.randombits.confluence.intercom.ConnectionBundle;
import org.randombits.confluence.intercom.IntercomStartup;
import org.randombits.confluence.intercom.LocalConnectionBundle;
import org.randombits.confluence.intercom.LocalIntercomListener;

public abstract class TrackerStartup extends IntercomStartup {

    private final Object[] values;

    public TrackerStartup( Object... values ) {
        this.values = values;
    }

    @Override
    protected ConnectionBundle createConnectionBundle() {
        return new LocalConnectionBundle( createConnections() );
    }

    @Override
    protected LocalIntercomListener createLocalIntercomListener() {
        // Do nothing
        return null;
    }

    private TrackerConnection[] createConnections() {
        if ( values == null )
            return null;

        TrackerConnection[] connections = new TrackerConnection[values.length];
        for ( int i = 0; i < values.length; i++ ) {
            connections[i] = new LocalTrackerConnection( values[i] );
        }

        return connections;
    }

}
