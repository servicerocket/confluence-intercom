package org.randombits.confluence.intercom.tracker;

/**
 * Provides an object of a particular type.
 * 
 * @author David Peterson
 */
public class LocalTrackerConnection implements TrackerConnection {

    private final Object value;

    /**
     * Constructs the provider with the specified value.
     * 
     * @param value
     *            The value to provide.
     */
    public LocalTrackerConnection( Object value ) {
        this.value = value;
    }

    /**
     * Returns the value as an instance of the specified type, if the current
     * value supports it. If the <code>type</code> is <code>null</code> or
     * the value does not support it, <code>null</code> is returned.
     */
    public <T> T getValue( Class<T> type ) {
        if ( type != null && type.isInstance( value ) )
            return type.cast( value );
        return null;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[ " + value + " ]";
    }
}
