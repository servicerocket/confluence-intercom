/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.intercom;

import com.atlassian.event.Event;
import com.atlassian.event.EventListener;
import com.atlassian.plugin.StateAware;

/**
 * This class should be extended and implemented by plugins wishing to act as
 * responders to a specific plugin's intercom requests. It should then be added
 * to the <code>atlassian-plugin.xml</code> file as an event listener.
 * 
 * @author David Peterson
 */
public abstract class IntercomStartup implements EventListener, StateAware {

    private static final Class<?>[] HANDLED_EVENTS = new Class<?>[0];
    
    private ConnectionBundle bundle;
    private LocalIntercomListener listener;

    public IntercomStartup() {
    }

    /**
     * Called to disable the intercom system.
     */
    public void disabled() {
        if ( bundle != null ) {
            LocalIntercom4.getInstance().removeLocalBundle( bundle );
            bundle = null;
        }
        
        if ( listener != null ) {
            LocalIntercom4.getInstance().removeLocalIntercomListener( listener );
            listener = null;
        }
        
        LocalIntercom4.getInstance().disable();
    }

    /**
     * Called to enable the intercom system.
     */
    public void enabled() {
        disabled();
        
        listener = createLocalIntercomListener();
        if ( listener != null )
            LocalIntercom4.getInstance().addLocalIntercomListener( listener );

        bundle = createConnectionBundle();
        if ( bundle != null )
            LocalIntercom4.getInstance().addLocalBundle( bundle );

        LocalIntercom4.getInstance().enable();
    }

    /**
     * If you wish to listen for local intercom events, return the listener
     * implementation here.
     * 
     * @return The local intercom listener to register.
     */
    protected abstract LocalIntercomListener createLocalIntercomListener();

    /**
     * If you wish to publish a {@link ConnectionBundle}, create it here. This
     * will be called whenever a new bundle should be created (typically once in
     * the plugin's lifecycle).
     * 
     * @return The new connection bundle.
     */
    protected abstract ConnectionBundle createConnectionBundle();

    public Class<?>[] getHandledEventClasses() {
        return HANDLED_EVENTS;
    }

    public void handleEvent( Event event ) {
        // Do nothing
    }
}
