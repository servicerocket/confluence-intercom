package org.randombits.confluence.intercom;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * This is the default implementation of {@link ConnectionBundle}. Most people
 * will use this class to provide connections to their {@link LocalIntercom4}
 * instance, and by extension, other intercom consumers.
 * 
 * @author David Peterson
 */
public class LocalConnectionBundle implements ConnectionBundle {
    private Set<Connection> connections;

    /**
     * Creates a new connection bundle, copying the list of connections provided
     * into the bundle.
     * 
     * @param connections
     *            The connection list.
     */
    public LocalConnectionBundle( Connection... connections ) {
        this( Arrays.asList( connections ) );
    }

    /**
     * Creates a new connection bundle, copying the contents of the provided
     * connection set into the bundle.
     * 
     * @param connections
     *            The connection set to copy.
     */
    public LocalConnectionBundle( Collection<Connection> connections ) {
        Set<Connection> copy = new java.util.HashSet<Connection>();
        copy.addAll( connections );
        this.connections = Collections.unmodifiableSet( copy );
    }

    /**
     * Returns an immutable set of the connections in this bundle.
     * 
     * @return The connection set.
     */
    public Connection[] getConnections() {
        return connections.toArray( new Connection[connections.size()] );
    }

    /**
     * Returns an immutable set of the connections in this bundle which
     * implement the specified connection class. May return <code>null</code>
     * if none are available, or the class is not supported.
     * 
     * @param <C>
     *            The connection type.
     * @param connectionType
     *            The connection type class to retrieve.
     * @return the set of connections from this bundle which implement the
     *         specified connection type.
     */
    public <C extends Connection> C[] getConnections( Class<C> connectionType ) {
        if ( connectionType != null ) {
            Set<C> filtered = new java.util.HashSet<C>();

            for ( Connection c : connections ) {
                if ( connectionType.isInstance( c ) )
                    filtered.add( ( C ) c );
            }

            C[] conns = ( C[] ) Array.newInstance( connectionType, filtered.size() );
            return filtered.toArray( conns );
        }

        return null;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[ " + connections + " ]";
    }

}
