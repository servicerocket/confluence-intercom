/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.confluence.intercom;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.randombits.facade.FacadeAssistant;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.spring.container.ContainerManager;

/**
 * A default implementation of the Intercom interface.
 * 
 * @author David Peterson
 */
public final class LocalIntercom4 implements Intercom4 {

    private static final Logger LOG = Logger.getLogger( LocalIntercom4.class );

    private static final LocalIntercom4 INSTANCE = new LocalIntercom4();

    public static LocalIntercom4 getInstance() {
        return INSTANCE;
    }

    private Set<ConnectionBundle> mLocalBundles;

    private Set<ConnectionBundle> mRemoteBundles;

    // The set of remote intercoms. Access via {@link #getRemoteIntercoms()}
    private Set<Intercom4> _remoteIntercoms;

    // The plugin this instance of LocalIntercom4 belongs to.
    private Plugin _plugin;

    private Set<LocalIntercomListener> listeners;

    private PluginAccessor pluginAccessor;

    private FacadeAssistant facadeAssistant;

    /**
     * Only tests should create new instances directly.
     */
    LocalIntercom4() {
        // Initialise the local bundle sets
        mLocalBundles = new java.util.HashSet<ConnectionBundle>( 5 );

        // Initialise the remote bundle sets.
        mRemoteBundles = new java.util.HashSet<ConnectionBundle>( 10 );

        pluginAccessor = ( PluginAccessor ) ContainerManager.getComponent( "pluginAccessor" );
        facadeAssistant = FacadeAssistant.getInstance();
    }

    /**
     * Adds the specified connection bundle to this local intercom. Any
     * connected intercoms will be notified of the addition, as well as any
     * listeners.
     * 
     * @param bundle
     * @return
     * @throws IllegalArgumentException
     *             if the bundle is a remote bundle.
     */
    public synchronized boolean addLocalBundle( ConnectionBundle bundle ) {
        if ( FacadeAssistant.getInstance().isLocalFacade( bundle ) )
            throw new IllegalArgumentException( "Remote connection bundles cannot be added to local intercoms." );

        // Initialise the bundle sets if necessary.
        if ( mLocalBundles == null ) {
            mLocalBundles = new java.util.HashSet<ConnectionBundle>( 5 );
        }

        if ( mLocalBundles.add( bundle ) ) {
            // First, notify local listeners
            notifyAddedConnectionBundle( bundle );
            // Then, add it to any remote intercoms.
            for ( Intercom4 intercom : getRemoteIntercoms() ) {
                try {
                    intercom.addRemoteBundle( bundle );
                } catch ( RuntimeException e ) {
                    LOG.error( "Unexpected exception: " + e.getLocalizedMessage(), e );
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Returns the set of bundles local to this intercom.
     * 
     * @return the local bundle set.
     */
    public ConnectionBundle[] getLocalBundles() {
        return mLocalBundles.toArray( new ConnectionBundle[mLocalBundles.size()] );
    }

    public synchronized boolean removeLocalBundle( ConnectionBundle bundle ) {
        if ( mLocalBundles.remove( bundle ) ) {
            notifyRemovedLocalBundle( bundle );
            return true;
        }
        return false;
    }

    private void notifyRemovedLocalBundle( ConnectionBundle bundle ) {
        // Notify listeners
        notifyRemovedConnectionBundle( bundle );
        // Notify remote intercoms
        for ( Intercom4 intercom : getRemoteIntercoms() ) {
            try {
                intercom.removeRemoteBundle( bundle );
            } catch ( RuntimeException e ) {
                LOG.error( "Unexpected exception: " + e.getLocalizedMessage(), e );
            }
        }
    }

    /**
     * Removes all local connection bundles from the local intercom.
     */
    public void clearLocalBundles() {
        Iterator<ConnectionBundle> i = mLocalBundles.iterator();
        while ( i.hasNext() ) {
            ConnectionBundle bundle = i.next();
            i.remove();
            notifyRemovedLocalBundle( bundle );
        }
    }

    /**
     * Adds the specified remote bundle. Any listeners will be notified of the
     * successful addition. If this intercom already contained a reference to
     * the bundle, this method has no effect and <code>false</code> is
     * returned.
     * 
     * @param bundle
     *            The remote bundle to add.
     * @return <code>true</code> if the bundle was successfully added.
     * @throws IllegalArgumentException
     *             if the bundle is not from a remote intercom.
     */
    public synchronized boolean addRemoteBundle( ConnectionBundle bundle ) {
        if ( !FacadeAssistant.getInstance().isLocalFacade( bundle ) )
            throw new IllegalArgumentException( "The collection bundle is not from a remote intercom." );

        // Attempt to add the bundle and notify listeners.
        if ( mRemoteBundles.add( bundle ) ) {
            notifyAddedConnectionBundle( bundle );
        }
        return false;
    }

    public ConnectionBundle[] getRemoteBundles() {
        return mRemoteBundles.toArray( new ConnectionBundle[mRemoteBundles.size()] );
    }

    /**
     * Removes the specified remove bundle, if present.
     * 
     * @param bundle
     *            The bundle to remove.
     * @return <code>true</code> if the bundle was present and successfully
     *         removed.
     */
    public synchronized boolean removeRemoteBundle( ConnectionBundle bundle ) {
        if ( mRemoteBundles.remove( bundle ) ) {
            // Notify listeners
            notifyRemovedConnectionBundle( bundle );
        }
        return false;
    }

    /**
     * Notifies any listeners that a connection bundle was added.
     * 
     * @param bundle
     *            the bundle which was added.
     */
    private void notifyAddedConnectionBundle( ConnectionBundle bundle ) {
        if ( listeners != null ) {
            for ( LocalIntercomListener l : listeners ) {
                try {
                    l.addedConnectionBundle( bundle );
                } catch ( RuntimeException e ) {
                    LOG
                            .error( "Unexpected RuntimeException while notifying intercom listeners: "
                                    + e.getMessage(), e );
                }
            }
        }
    }

    /**
     * Notifies listeners that a bundle has been removed.
     * 
     * @param bundle
     */
    private void notifyRemovedConnectionBundle( ConnectionBundle bundle ) {
        if ( listeners != null ) {
            for ( LocalIntercomListener l : listeners ) {
                l.removedConnectionBundle( bundle );
            }
        }
    }

    Set<Intercom4> getRemoteIntercoms() {
        if ( _remoteIntercoms == null ) {
            _remoteIntercoms = new java.util.HashSet<Intercom4>();

            Collection<Plugin> plugins = pluginAccessor.getPlugins();
            Intercom4 intercom;

            for ( Plugin plugin : plugins ) {
                intercom = findRemoteIntercom( plugin );
                if ( intercom != null ) {
                    registerIntercom( intercom );
                }
            }
        }

        return _remoteIntercoms;
    }

    private Intercom4 findRemoteIntercom( Plugin plugin ) {
        if ( plugin.isDynamicallyLoaded() ) {
            Class<?> remoteIntercomClass;
            try {
                remoteIntercomClass = plugin.loadClass( LocalIntercom4.class.getName(), Object.class );
                // Check if this is the local plugin intercom.
                if ( LocalIntercom4.class == remoteIntercomClass ) {
                    _plugin = plugin;
                    return null;
                }
            } catch ( ClassNotFoundException e ) {
                return null;
            }

            if ( remoteIntercomClass != null ) {
                try {
                    // Retrieve the singleton instance of the remote
                    // 'LocalIntercom'.
                    Method getInstance = remoteIntercomClass.getMethod( "getInstance" );
                    Object remoteIntercom = getInstance.invoke( null );
                    // Next, prepare the object as an instance of Intercom4.
                    return facadeAssistant.prepareObject( remoteIntercom, Intercom4.class );
                } catch ( NoSuchMethodException e ) {
                    LOG.warn( e );
                } catch ( IllegalAccessException e ) {
                    LOG.warn( e );
                } catch ( InvocationTargetException e ) {
                    LOG.warn( e );
                }
            }
        }
        return null;
    }

    /**
     * Turns on the intercom, connecting it with any remote intercoms which may
     * already exist.
     */
    public synchronized void enable() {
        // Register ourselves with all known intercoms.
        for ( Intercom4 intercom : getRemoteIntercoms() ) {
            intercom.registerIntercom( this );
        }
    }

    public synchronized void disable() {
        if ( _remoteIntercoms != null ) {
            for ( Intercom4 intercom : _remoteIntercoms ) {
                // Remove ourself from the remote intercom.
                intercom.deregisterIntercom( this );
            }
            _remoteIntercoms.clear();
            _remoteIntercoms = null;
        }

        mRemoteBundles.clear();
    }

    private void addRemoteBundles( ConnectionBundle[] remoteBundles ) {
        for ( ConnectionBundle bundle : remoteBundles ) {
            addRemoteBundle( bundle );
        }
    }

    /**
     * Removes the specified set of remote bundles, if they are present. Any
     * listeners are notified of the removal.
     * 
     * @param connectionBundles
     *            The set of bundles to remove.
     */
    private void removeRemoteBundles( ConnectionBundle[] connectionBundles ) {
        for ( ConnectionBundle bundle : connectionBundles ) {
            removeRemoteBundle( bundle );
        }
    }

    /**
     * Registers the remote intercom with this intercom. Will also add any
     * existing connection bundles to the set of remote bundles in this
     * intercom.
     * 
     * @param intercom
     *            The remote intercom.
     * @return <code>false</code> if the intercom was already registered.
     */
    public synchronized boolean registerIntercom( Intercom4 intercom ) {
        if ( intercom == this )
            return false;

        if ( _remoteIntercoms == null )
            _remoteIntercoms = new java.util.HashSet<Intercom4>();

        if ( _remoteIntercoms.add( intercom ) ) {
            try {
                addRemoteBundles( intercom.getLocalBundles() );
                return true;
            } catch ( RuntimeException e ) {
                LOG.error( "Unexpected exception while registering intercom '" + intercom + "' with '" + this
                        + "'.", e );
            }
        }
        return false;
    }

    /**
     * Deregisters the specified intercom from this intercom.
     */
    public synchronized boolean deregisterIntercom( Intercom4 intercom ) {
        if ( intercom == this )
            return false;

        assert FacadeAssistant.getInstance().isFacade( intercom );
        if ( _remoteIntercoms != null ) {
            if ( _remoteIntercoms.remove( intercom ) ) {
                try {
                    removeRemoteBundles( intercom.getLocalBundles() );
                    return true;
                } catch ( RuntimeException e ) {
                    LOG.error( "Unexpected exception while deregistering intercom '" + intercom + "' from '"
                            + this + "'.", e );
                }
            }
        }
        return false;
    }

    /**
     * Returns the current plugin. This may be null if the plugin set has not
     * been enabled.
     * 
     * @return The plugin.
     * @see #enable();
     */
    public Plugin getPlugin() {
        return _plugin;
    }

    @Override
    public String toString() {
        StringBuffer out = new StringBuffer();

        out.append( "LocalIntercom4" );

        out.append( " [plugin: " );
        if ( _plugin != null )
            out.append( _plugin.getName() ).append( " (" ).append( _plugin.getKey() ).append( ")" );
        else
            out.append( "<unknown>" );
        out.append( "]" );

        return out.toString();
    }

    /**
     * Adds the listener to the intercom.
     * 
     * @param listener
     *            The intercom listener.
     */
    public void addLocalIntercomListener( LocalIntercomListener listener ) {
        if ( listeners == null )
            listeners = new java.util.HashSet<LocalIntercomListener>();

        listeners.add( listener );
    }

    /**
     * Removes the listener from the intercom.
     * 
     * @param listener
     *            The intercom listener.
     */
    public void removeLocalIntercomListener( LocalIntercomListener listener ) {
        if ( listeners != null )
            listeners.remove( listener );
    }

    /**
     * Returns the set of Connections of the specified type, as defined by the
     * current remote and local connection set.
     * 
     * @param <C>
     *            The connection type.
     * @param connectionType
     *            The class type.
     * @return The set of Connections.
     */
    public synchronized <C extends Connection> Set<C> findConnections( Class<C> connectionType ) {
        Set<C> filtered = new java.util.HashSet<C>( Math.max( mLocalBundles.size() + mRemoteBundles.size(), 10 ) );
        addConnections( filtered, mLocalBundles, connectionType );
        addConnections( filtered, mRemoteBundles, connectionType );
        return filtered;
    }

    private <C extends Connection> void addConnections( Set<C> filtered, Set<ConnectionBundle> bundles,
            Class<C> connectionType ) {
        for ( ConnectionBundle bundle : bundles ) {
            try {
                C[] connections = bundle.getConnections( connectionType );
                if ( connections != null )
                    filtered.addAll( Arrays.asList( connections ) );
            } catch ( NullPointerException e ) {
                // Handle a bug in an older version of Intercom which may be in
                // use by other plugins
                LOG.warn( "A remote connection bundle is using a buggy version of Intercom: " + bundle, e );
            }
        }
    }
}
