package org.randombits.confluence.intercom.thing;

/**
 * Provides an object of a particular type.
 * 
 * @author David Peterson
 */
public abstract class LocalThingConnection<T> implements ThingConnection<T> {

    private T thing;

    /**
     * Constructs the provider with the specified thing.
     * 
     * @param thing
     *            The thing to provide.
     */
    public LocalThingConnection( T thing ) {
        this.thing = thing;
    }

    public T getThing() {
        return thing;
    }
    
    @Override public String toString() {
        return getClass().getSimpleName() + "[ " + thing + " ]";
    }
}
