package org.randombits.confluence.intercom.thing;

import org.randombits.confluence.intercom.Connection;

/**
 * Implementors should provide an interface subclass of this class, as well as a specific type
 * to return.
 * 
 * @author David Peterson
 * 
 * @param <T>
 *            The type of object being provided.
 */
public interface ThingConnection<T> extends Connection {
    /**
     * Returns the object being provided.
     * 
     * @return The value being provided.
     */
    T getThing();
}
