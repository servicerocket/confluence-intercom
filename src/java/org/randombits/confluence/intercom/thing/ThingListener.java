package org.randombits.confluence.intercom.thing;

public interface ThingListener<T> {
    /**
     * Called when a new thing is provided, usually via adding a connection.
     * 
     * @param thing
     *            The new thing.
     */
    public void thingProvided( T thing );

    /**
     * Called when a thing is removed, usually via removing a connection.
     * 
     * @param thing
     */
    public void thingRemoved( T thing );
}
