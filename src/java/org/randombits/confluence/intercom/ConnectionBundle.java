package org.randombits.confluence.intercom;

import org.randombits.facade.Facadable;
import org.randombits.facade.ArrayTypeParameter;

/**
 * Represents a set of connections being provided as a fixed bundle. Once a
 * bundle is created, the set of connections it contains will not change. In
 * general, interested parties should keep their own copy of whatever
 * connections are returned from the methods of choice, since internally there
 * may be reasonable effort required to return the result.
 * 
 * @author David Peterson
 */
@Facadable
public interface ConnectionBundle {
    /**
     * Returns an disposable array of the connections in this bundle.
     * 
     * @return the connection set.
     */
    public Connection[] getConnections();

    /**
     * Returns an disposable array of the connections that match the specified
     * type contained in this bundle. This will never return <code>null</code>.
     * The results of this method are likely to be calculated on request, so
     * caching the returned set is recommended, particularly since the set
     * returned will never change over the lifespan of this class.
     * 
     * @param <C>
     *            The connection type.
     * @param connectionType
     *            The connection class.
     * @return The set of connections which implement the specified type.
     */
    @ArrayTypeParameter( 0 )
    public <C extends Connection> C[] getConnections( @Facadable Class<C> connectionType );
}
